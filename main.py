from typing import List


class Solution:
    def solveNQueens(self, n: int) -> List[List[str]]:
        res = []
        # 畫出板子
        board = ['.' * n for _ in range(n)]

        def isValid(board, row, col):
            # 檢查皇后位置
            # 檢查第 row 行 第 col 列是否可以放皇后
            for row_index in range(row):
                # 判斷當前行是否放了皇后
                if row_index == row:
                    if 'Q' in board[row_index]:
                        return False
                # 遍歷每行，第 col 列是否已經放了皇后
                if 'Q' == board[row_index][col]:
                    return False

            # 判斷左斜上方是否有皇后
            tmp_row, tmp_col = row, col
            while tmp_row > 0 and tmp_col > 0:
                tmp_row -= 1
                tmp_col -= 1
                if 'Q' in board[tmp_row][tmp_col]:
                    return False

            # 判斷右斜上方是否有皇后
            tmp_row, tmp_col = row, col
            while tmp_row > 0 and tmp_col < n - 1:
                tmp_row -= 1
                tmp_col += 1
                if 'Q' in board[tmp_row][tmp_col]:
                    return False

            return True

        def replace_char(string, char, index):
            string = list(string)
            string[index] = char
            return ''.join(string)

        def backtrack(board, row):
            # 1.完成條件
            if row == len(board):
                res.append(list(board[:]))
                return

            # 2.檢查
            for col in range(n):
                # 檢查
                if not isValid(board, row, col):
                    continue
                # 3.更新 row
                board[row] = replace_char(board[row], 'Q', col)
                backtrack(board, row + 1)
                board[row] = replace_char(board[row], '.', col)
        # 開始
        backtrack(board, 0)
        return res


if __name__ == "__main__":
    n = 4
    print(Solution().solveNQueens(n))
